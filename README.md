 # Fronted Developer Challenge
Este proyecto ha sido creado para la Prueba T�cnica de Admisi�n a la Compa��a multinacional Rappi.
## Developer
* .Pilyn Merch�n C�ceres

## Requirementos
* Tener internet para descargar algunas librer�as Online,utilizadas en el proyecto.
* Publicar sitio en Servidores Web, puede ser en Xampp basado en Apache Server o IIS de Microsoft Windows.
* Navegadores recomendados: Edge,IE 8,Chrome,Opera.
## Modo de Uso
```
Dentro de la carpeta Demos, se inicia la p�gina index.html
una vez abierta la p�gina, en el men� se despliegan las diferentes categor�as, al dar click sobre alguna categor�a se cargan los productos asociados a esta.
Al seleccionar los filtros, cada uno carga la informaci�n sobre la categor�a que ha sido seleccionada.
Al seleccionar sobre un producto, la opci�n a�adir a la canasta, el contador de productos se va incrementando, si se desea aumentar la cantidad del mismo producto, se da de nuevo click a la opci�n a�adir a la canasta, si se desea disminuir la cantidad de un producto, se da click sobre el panel de productos seleccionados, al �cono con el car�cter(-).
El filtro por nombre, es visualizado solamente al seleccionar un subnivel final, si se selecciona otra opci�n de filtro, desaparece.



